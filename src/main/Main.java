package main;

import controlador.*;
import modelo.*;
import vista.*;
import vista.articulo.*;
import vista.cliente.*;
import vista.pedido.*;

public class Main {

	public static void main(String[] args) {

		// Crear controladores como variables locales del main

		ControladorArticulo controladorArticulo = new ControladorArticulo();
		ControladorCliente controladorCliente = new ControladorCliente();
		ControladorPedido controladorPedido = new ControladorPedido();
		
		//modelos
		ModeloArticulo modeloArticulo = new ModeloArticulo();
		ModeloCliente modeloCliente = new ModeloCliente();
		ModeloPedido modeloPedido = new ModeloPedido();
		ModeloDetallePedido modeloDetallePedido = new ModeloDetallePedido();
		
		//Ventanas
		//Principal
		Principal principal = new Principal();
		principal.setControladorArticulo(controladorArticulo);
		principal.setControladorCliente(controladorCliente);
		principal.setControladorPedido(controladorPedido);

		// Articulo
		// Crear ventanas como variables locales del main
		GestionArticulo gestionArticulo = new GestionArticulo(principal, true);
		gestionArticulo.setControladorArticulo(controladorArticulo);

		NuevoArticulo nuevoArticulo = new NuevoArticulo(gestionArticulo, true);
		nuevoArticulo.setControladorArticulo(controladorArticulo);

		BorrarArticulo borrarArticulo = new BorrarArticulo(gestionArticulo, true);
		borrarArticulo.setControladorArticulo(controladorArticulo);
		
		ConsultarArticulo consultarArticulo = new ConsultarArticulo(gestionArticulo, true);
		consultarArticulo.setControladorArticulo(controladorArticulo);
		
		ListarArticulo listarArticulo = new ListarArticulo(gestionArticulo, true);
		listarArticulo.setControladorArticulo(controladorArticulo);
		
		//rellenar controladorArticulo
		controladorArticulo.setNuevoArticulo(nuevoArticulo);
		controladorArticulo.setGestionArticulo(gestionArticulo);
		controladorArticulo.setBorrarArticulo(borrarArticulo);
		controladorArticulo.setConsultarArticulo(consultarArticulo);
		controladorArticulo.setListarArticulo(listarArticulo);
		controladorArticulo.setModeloArticulo(modeloArticulo);
		// Cliente
		// Crear ventanas como variables locales del main
		

		//creacion de ventanas de la gestion de Clientes y asignar controladores
		GestionCliente gestionCliente = new GestionCliente(principal, true);
		gestionCliente.setControladorCliente(controladorCliente);
		
		BorrarCliente borrarCliente = new BorrarCliente(gestionCliente, true);
		borrarCliente.setControladorCliente(controladorCliente);
		
		NuevoCliente nuevoCliente = new NuevoCliente(gestionCliente, true);
		nuevoCliente.setControladorCliente(controladorCliente);
		
		ListarClientes listarClientes = new ListarClientes(gestionCliente, true);
		listarClientes.setControladorCliente(controladorCliente);
		
		ListarClientesPedidos listarClientesPedidos = new ListarClientesPedidos(gestionCliente, true);
		listarClientesPedidos.setControladorCliente(controladorCliente);
		
		ConsultarCliente consultarCliente = new ConsultarCliente(gestionCliente, true);
		consultarCliente.setControladorCliente(controladorCliente);
		
		
		//rellenar controladorCliente
		controladorCliente.setNuevoCliente(nuevoCliente);
		controladorCliente.setGestionCliente(gestionCliente);
		controladorCliente.setBorrarCliente(borrarCliente);
		controladorCliente.setConsultarCliente(consultarCliente);
		controladorCliente.setListarClientes(listarClientes);
		controladorCliente.setListarClientesPedidos(listarClientesPedidos);
		controladorCliente.setModeloCliente(modeloCliente);
		
		// Pedido
		// Crear ventanas como variables locales del main
		

		//creacion de ventanas de la gestion de Pedidos y asignar controladores
		GestionPedido gestionPedido = new GestionPedido(principal, true);
		gestionPedido.setControladorPedido(controladorPedido);
		
		BorrarPedido borrarPedido = new BorrarPedido(gestionPedido, true);
		borrarPedido.setControladorPedido(controladorPedido);
		
		NuevoPedido nuevoPedido = new NuevoPedido(gestionPedido, true);
		nuevoPedido.setControladorPedido(controladorPedido);
		
		ListarPedidos listarPedidos = new ListarPedidos(gestionPedido, true);
		listarPedidos.setControladorPedido(controladorPedido);
		
		ConsultarPedido consultarPedido = new ConsultarPedido(gestionPedido, true);
		consultarPedido.setControladorPedido(controladorPedido);
		
		
		//rellenar controladorPedido
		controladorPedido.setNuevoPedido(nuevoPedido);
		controladorPedido.setGestionPedido(gestionPedido);
		controladorPedido.setBorrarPedido(borrarPedido);
		controladorPedido.setConsultarPedido(consultarPedido);
		controladorPedido.setListarPedidos(listarPedidos);
		controladorPedido.setModeloPedido(modeloPedido);
		controladorPedido.setModeloCliente(modeloCliente);
		
		//abrir la ventana principal
		principal.setVisible(true);

	}

}
