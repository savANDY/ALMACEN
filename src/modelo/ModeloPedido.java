package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.*;

import java.sql.Statement;

public class ModeloPedido extends Conectar{
	
	public ModeloPedido() {
		super();
	}

	public ArrayList<Pedido> seleccionarTodos() throws Exception {
		
		Pedido pedido;
		ArrayList<Pedido>  pedidos=new ArrayList<Pedido>();
				
		try {
			Statement st = cn.createStatement();
			
			ResultSet rs = st.executeQuery("SELECT * FROM PEDIDOS ");// ejecuta

			//pasar de ResultSet a ArrayList
			
			while (rs.next()){
				pedido=new Pedido();
				pedido.setId(Integer.parseInt(rs.getString(1)));
				pedido.setIdCliente(rs.getString(2));
				pedido.setFecha(rs.getDate(3));
				pedido.setCodPostal(rs.getString(4));

				pedidos.add(pedido);
			}
			
			return pedidos;
			
		} catch (Exception e) {
			throw e;

		} 
	}

	public ArrayList<DetallesPedido> seleccionarDetallesPedido(int idPedido) throws Exception {
		
		
		PreparedStatement pst = cn.prepareStatement("SELECT DP.*,A.NOMBRE FROM DETALLESPEDIDOS DP,ARTICULOS A "
				+ " WHERE idPedido=? AND"
				+ " DP.IDARTICULO=A.ID");
		
		pst.setInt(1, idPedido);
		ResultSet rs = pst.executeQuery();
		
		ArrayList<DetallesPedido>  detallesPedidos=new ArrayList<DetallesPedido>();
		
		
		//recorrer los detalles de ese pedido
		
		while (rs.next()){
			
			DetallesPedido detallesPedido=new DetallesPedido();
			
			detallesPedido.setIdPedido(Integer.parseInt(rs.getString(1)));
			detallesPedido.setIdArticulo(Integer.parseInt(rs.getString(2)));
			detallesPedido.setCantidad(Integer.parseInt(rs.getString(3)));
			
			Articulo articulo=new Articulo();
			articulo.setNombre(rs.getString(4));
			detallesPedido.setArticulo(articulo);
			
			detallesPedidos.add(detallesPedido);
		}
		return(detallesPedidos);

	}
}
