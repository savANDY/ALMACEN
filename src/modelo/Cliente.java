package modelo;

import java.util.ArrayList;

public class Cliente {
	
	private String id;
	private String  nombre;
	private String   direccion;
	private String codPostal;
	private String telefono;
	
	private ArrayList<Pedido>  pedidos; //hace N pedidos

	public Cliente(String id, String nombre, String direccion, String codPostal, String telefono,
			ArrayList<Pedido> pedidos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.codPostal = codPostal;
		this.telefono = telefono;
		this.pedidos = pedidos;
	}

	public Cliente() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public ArrayList<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	
}
