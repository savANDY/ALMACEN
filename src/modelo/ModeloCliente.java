package modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ModeloCliente extends Conectar {

	public ModeloCliente() {
		super();
	}

	public Cliente seleccionarDatosCliente(String id) throws Exception {
		PreparedStatement pst;
		Cliente cliente=new Cliente();
		
		try {
			pst = cn.prepareStatement("SELECT * FROM CLIENTES WHERE id=?");
			pst.setString(1, id);

			ResultSet rs = pst.executeQuery();// ejecuta

			while (rs.next()) { // coge el titulo que es UNO SOLO
				
				cliente.setId(rs.getString(1));
				cliente.setNombre(rs.getString(2));
				cliente.setDireccion(rs.getString(3));
				cliente.setCodPostal(rs.getString(4));
				cliente.setTelefono(rs.getString(5));
			}
			return cliente;
			
		} catch (Exception e) {
			throw e;

		} 
		
		
	}
	
	public ArrayList<Cliente> seleccionarTodos() {

		Statement st;
		try {
			st = cn.createStatement();

			ResultSet rs = st.executeQuery("SELECT * FROM CLIENTES ");

			// pasar de ResultSet a ArrayList

			ArrayList<Cliente> clientes = new ArrayList<Cliente>();

			while (rs.next()) {

				Cliente cliente = new Cliente();
				cliente.setId(rs.getString(1));
				cliente.setNombre(rs.getString(2));
				cliente.setDireccion(rs.getString(3));
				cliente.setCodPostal(rs.getString(4));
				cliente.setTelefono(rs.getString(5));
				clientes.add(cliente);
			}
			return clientes;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public void insertar(Cliente cliente) throws Exception {

		PreparedStatement pst;
		try {
			pst = cn.prepareStatement(
					"INSERT INTO CLIENTES(id,nombre,direccion,codpostal,telefono) VALUES (?,?,?,?,?)");
			
			pst.setString(1, cliente.getId());
			pst.setString(2, cliente.getNombre());
			pst.setString(3, cliente.getDireccion());
			pst.setString(4, cliente.getCodPostal());
			pst.setString(5, cliente.getTelefono());

			System.out.println(pst);
			pst.execute();
			System.out.println("Alumno insertado correctamente");
		} catch (Exception e) {
			throw e;
		}

	}

	public void borrar(String id) {

		try {
			PreparedStatement pst = cn.prepareStatement("DELETE FROM clientes WHERE id = ?");
			pst.setString(1, id);
			pst.execute();// ejecuta
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int modificar(Cliente cliente) {
		int lineascambiadas;
		try {
			Statement st = super.cn.createStatement();
			lineascambiadas = st.executeUpdate("UPDATE clientes " + "SET nombre='" + cliente.getNombre() + "'"
					+ ",direccion='" + cliente.getDireccion() + "'" + ",codpostal='" + cliente.getCodPostal() + "'"
					+ ",telefono='" + cliente.getTelefono() + "'" + " WHERE id=" + cliente.getId());
			return lineascambiadas;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}
	

	public String seleccionarId(String id) throws Exception {
		try {

			PreparedStatement pst = cn.prepareStatement("SELECT NOMBRE,DIRECCION FROM clientes WHERE id = ?");
			pst.setString(1, id);

			// System.out.println(pst);

			ResultSet rs = pst.executeQuery();// ejecuta
			String nomDirecc = "";

			while (rs.next()) {
				nomDirecc = rs.getString(1) + " " + rs.getString(2);
			}

			return nomDirecc;

		} catch (Exception ex) {
			throw ex;
		}
	}

	public String seleccionarPorNombre(String nombre, String direccion) throws Exception {

		try {

			PreparedStatement pst = cn.prepareStatement("SELECT ID from clientes WHERE nombre = ? and direccion=?");

			pst.setString(1, nombre);
			pst.setString(2, direccion);

			// System.out.println(pst);

			ResultSet rs = pst.executeQuery();// ejecuta
			String id = null;

			while (rs.next()) {
				id = rs.getString(1);
			}
			return id;

		} catch (Exception ex) {
			throw ex;
		}

	}

	public Cliente select(String idSocio) {
		PreparedStatement ps;
		Cliente cliente;
		try {
			ps = cn.prepareStatement("select * from clientes where id = ?");
			ps.setString(1, idSocio);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				cliente = new Cliente();
				cliente.setId(rs.getString(1));
				cliente.setNombre(rs.getString(2));
				cliente.setDireccion(rs.getString(3));
				cliente.setCodPostal(rs.getString(4));
				cliente.setTelefono(rs.getString(5));
				return cliente;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}



	public ArrayList<String> selectDirecciones() {
		ArrayList<String> direcciones = new ArrayList<String>();
		try {
			Statement st = super.cn.createStatement();
			ResultSet rs = st.executeQuery("select distinct direccion from clientes");

			while (rs.next()) {
				direcciones.add(rs.getString(1));
			}
			return direcciones;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return direcciones;
	}
	
	

}
