package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.*;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	private ControladorArticulo controladorArticulo;
	private ControladorCliente controladorCliente;
	private ControladorPedido controladorPedido;

	public ControladorArticulo getControladorArticulo() {
		return controladorArticulo;
	}

	public void setControladorArticulo(ControladorArticulo controladorArticulo) {
		this.controladorArticulo = controladorArticulo;
	}

	public ControladorCliente getControladorCliente() {
		return controladorCliente;
	}

	public void setControladorCliente(ControladorCliente controladorCliente) {
		this.controladorCliente = controladorCliente;
	}

	public ControladorPedido getControladorPedido() {
		return controladorPedido;
	}

	public void setControladorPedido(ControladorPedido controladorPedido) {
		this.controladorPedido = controladorPedido;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("GESTION ALMACEN");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(103, 11, 227, 41);
		contentPane.add(lblNewLabel);
		
		JButton articulos = new JButton("Gesti\u00F3n Articulos");
		articulos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controladorArticulo.abrirGestionArticulo();
			}
		});
		articulos.setFont(new Font("Tahoma", Font.BOLD, 12));
		articulos.setBounds(96, 83, 234, 23);
		contentPane.add(articulos);
		
		JButton clientes = new JButton("Gesti\u00F3n Clientes");
		clientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controladorCliente.abrirGestionCliente();
			}
		});
		clientes.setFont(new Font("Tahoma", Font.BOLD, 12));
		clientes.setBounds(96, 129, 234, 23);
		contentPane.add(clientes);
		
		JButton pedidos = new JButton("Gesti\u00F3n Pedidos");
		pedidos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controladorPedido.abriGestionPedido();
			}
		});
		pedidos.setFont(new Font("Tahoma", Font.BOLD, 12));
		pedidos.setBounds(96, 175, 234, 23);
		contentPane.add(pedidos);
	}
}
