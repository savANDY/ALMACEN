package vista.pedido;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPedido;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionPedido extends JDialog {

	private final JPanel contentPanel = new JPanel();
	
	private ControladorPedido controladorPedido;

	public ControladorPedido getControladorPedido() {
		return controladorPedido;
	}

	public void setControladorPedido(ControladorPedido controladorPedido) {
		this.controladorPedido = controladorPedido;
	}

	/**
	 * Create the dialog.
	 */
	public GestionPedido(JFrame parent, boolean modal) {
		
		super(parent,modal);
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 261);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		{
			JButton nuevo = new JButton("NUEVO PEDIDO");
			nuevo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					controladorPedido.abrirNuevoPedido();
				}
			});
			nuevo.setBounds(49, 77, 189, 23);
			contentPanel.add(nuevo);
		}
		{
			JButton borrar = new JButton("BORRAR PEDIDO");
			borrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					controladorPedido.abrirBorrarPedido();
				}
			});
			borrar.setBounds(49, 123, 189, 23);
			contentPanel.add(borrar);
		}
		{
			JButton consultas = new JButton("CONSULTAS  PEDIDOS");
			consultas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					controladorPedido.abrirConsultarPedido();
				}
			});
			consultas.setBounds(49, 168, 189, 23);
			contentPanel.add(consultas);
		}
		{
			JLabel lblNewLabel = new JLabel("GESTION DE PEDIDOS");
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
			lblNewLabel.setBounds(94, 28, 275, 23);
			contentPanel.add(lblNewLabel);
		}
		
		JButton listar = new JButton("LISTAR PEDIDOS");
		listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				controladorPedido.abrirListarPedidos();
			}
		});
		listar.setBounds(49, 214, 189, 23);
		contentPanel.add(listar);
	}
}
