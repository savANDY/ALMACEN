package vista.cliente;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorCliente;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;

public class ListarClientesPedidos extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable tablaCli;
	private JTable tablaPed;

	private ControladorCliente controladorCliente;
	private JTable tablaDeta;

	public ControladorCliente getControladorCliente() {
		return controladorCliente;
	}

	public void setControladorCliente(ControladorCliente controladorCliente) {
		this.controladorCliente = controladorCliente;
	}

	/**
	 * Create the dialog.
	 */
	public ListarClientesPedidos( JDialog parent,boolean modal   ) {
		
		super(parent,modal);
		setBounds(100, 100, 683, 436);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 667, 397);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 87, 320, 190);
		contentPanel.add(scrollPane);
		
		tablaCli = new JTable();
		scrollPane.setViewportView(tablaCli);
		
		JLabel lblPedidosPorCliente = new JLabel("PEDIDOS POR CLIENTE");
		lblPedidosPorCliente.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPedidosPorCliente.setBounds(251, 11, 285, 14);
		contentPanel.add(lblPedidosPorCliente);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(383, 87, 274, 120);
		contentPanel.add(scrollPane_1);
		
		tablaPed = new JTable();
		scrollPane_1.setViewportView(tablaPed);
		
		JLabel lblClientes = new JLabel("CLIENTES");
		lblClientes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblClientes.setBounds(131, 54, 118, 14);
		contentPanel.add(lblClientes);
		
		JLabel lblPedidosDelCliente = new JLabel("PEDIDOS DEL CLIENTE");
		lblPedidosDelCliente.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPedidosDelCliente.setBounds(449, 54, 189, 14);
		contentPanel.add(lblPedidosDelCliente);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(387, 262, 270, 107);
		contentPanel.add(scrollPane_2);
		
		tablaDeta = new JTable();
		scrollPane_2.setViewportView(tablaDeta);
		
		JLabel lblDetallesDelPedido = new JLabel("DETALLES DEL PEDIDO");
		lblDetallesDelPedido.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDetallesDelPedido.setBounds(449, 234, 189, 14);
		contentPanel.add(lblDetallesDelPedido);
	}
}
